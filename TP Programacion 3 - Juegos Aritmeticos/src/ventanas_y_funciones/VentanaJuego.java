package ventanas_y_funciones;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import ventanas_y_funciones.VentanaJuego;

public class VentanaJuego {

	private JFrame frmJuegosAritmticos;
	private JTextField casilla1;
	private JTextField casilla2;
	private JTextField casilla3;
	private JTextField casilla4;
	private JTextField casilla5;
	private JTextField casilla6;
	private JTextField casilla7;
	private JTextField casilla8;
	private JTextField casilla9;
	private JTextField casilla10;
	private JTextField casilla11;
	private JTextField casilla12;
	private JTextField casilla13;
	private JTextField casilla14;
	private JTextField casilla15;
	private JTextField casilla16;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaJuego window = new VentanaJuego();
					window.frmJuegosAritmticos.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VentanaJuego() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("Error setting Native LAF: " + e);
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmJuegosAritmticos = new JFrame();
		frmJuegosAritmticos.setTitle("Juegos Aritm\u00E9ticos - Programaci\u00F3n 3");
		frmJuegosAritmticos.getContentPane().setBackground(new Color(0, 128, 128));
		frmJuegosAritmticos.setBounds(100, 100, 568, 472);
		frmJuegosAritmticos.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmJuegosAritmticos.getContentPane().setLayout(null);

		casilla1 = new JTextField();
		casilla1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla1.setHorizontalAlignment(SwingConstants.CENTER);
		casilla1.setBackground(Color.WHITE);
		casilla1.setBounds(69, 85, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla1);
		casilla1.setColumns(10);

		casilla2 = new JTextField();
		casilla2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla2.setHorizontalAlignment(SwingConstants.CENTER);
		casilla2.setColumns(10);
		casilla2.setBackground(Color.WHITE);
		casilla2.setBounds(129, 85, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla2);

		casilla3 = new JTextField();
		casilla3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla3.setHorizontalAlignment(SwingConstants.CENTER);
		casilla3.setColumns(10);
		casilla3.setBackground(Color.WHITE);
		casilla3.setBounds(189, 85, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla3);

		casilla4 = new JTextField();
		casilla4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla4.setHorizontalAlignment(SwingConstants.CENTER);
		casilla4.setColumns(10);
		casilla4.setBackground(Color.WHITE);
		casilla4.setBounds(249, 85, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla4);

		casilla5 = new JTextField();
		casilla5.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla5.setHorizontalAlignment(SwingConstants.CENTER);
		casilla5.setColumns(10);
		casilla5.setBackground(Color.WHITE);
		casilla5.setBounds(69, 146, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla5);

		casilla6 = new JTextField();
		casilla6.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla6.setHorizontalAlignment(SwingConstants.CENTER);
		casilla6.setColumns(10);
		casilla6.setBackground(Color.WHITE);
		casilla6.setBounds(129, 146, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla6);

		casilla7 = new JTextField();
		casilla7.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla7.setHorizontalAlignment(SwingConstants.CENTER);
		casilla7.setColumns(10);
		casilla7.setBackground(Color.WHITE);
		casilla7.setBounds(189, 146, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla7);

		casilla8 = new JTextField();
		casilla8.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla8.setHorizontalAlignment(SwingConstants.CENTER);
		casilla8.setColumns(10);
		casilla8.setBackground(Color.WHITE);
		casilla8.setBounds(249, 146, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla8);

		casilla9 = new JTextField();
		casilla9.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla9.setHorizontalAlignment(SwingConstants.CENTER);
		casilla9.setColumns(10);
		casilla9.setBackground(Color.WHITE);
		casilla9.setBounds(69, 207, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla9);

		casilla10 = new JTextField();
		casilla10.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla10.setHorizontalAlignment(SwingConstants.CENTER);
		casilla10.setColumns(10);
		casilla10.setBackground(Color.WHITE);
		casilla10.setBounds(129, 207, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla10);

		casilla11 = new JTextField();
		casilla11.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla11.setHorizontalAlignment(SwingConstants.CENTER);
		casilla11.setColumns(10);
		casilla11.setBackground(Color.WHITE);
		casilla11.setBounds(189, 207, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla11);

		casilla12 = new JTextField();
		casilla12.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla12.setHorizontalAlignment(SwingConstants.CENTER);
		casilla12.setColumns(10);
		casilla12.setBackground(Color.WHITE);
		casilla12.setBounds(249, 207, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla12);

		casilla13 = new JTextField();
		casilla13.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla13.setHorizontalAlignment(SwingConstants.CENTER);
		casilla13.setColumns(10);
		casilla13.setBackground(Color.WHITE);
		casilla13.setBounds(69, 268, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla13);

		casilla14 = new JTextField();
		casilla14.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla14.setHorizontalAlignment(SwingConstants.CENTER);
		casilla14.setColumns(10);
		casilla14.setBackground(Color.WHITE);
		casilla14.setBounds(129, 268, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla14);

		casilla15 = new JTextField();
		casilla15.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla15.setHorizontalAlignment(SwingConstants.CENTER);
		casilla15.setColumns(10);
		casilla15.setBackground(Color.WHITE);
		casilla15.setBounds(189, 268, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla15);

		casilla16 = new JTextField();
		casilla16.setFont(new Font("Tahoma", Font.PLAIN, 18));
		casilla16.setHorizontalAlignment(SwingConstants.CENTER);
		casilla16.setColumns(10);
		casilla16.setBackground(Color.WHITE);
		casilla16.setBounds(249, 268, 50, 50);
		frmJuegosAritmticos.getContentPane().add(casilla16);

		JLabel titulo = new JLabel("Juegos Aritm\u00E9ticos");
		titulo.setForeground(Color.WHITE);
		titulo.setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
		titulo.setBounds(184, 11, 187, 31);
		frmJuegosAritmticos.getContentPane().add(titulo);

		JLabel fila1 = new JLabel("??");
		fila1.setForeground(Color.WHITE);
		fila1.setHorizontalAlignment(SwingConstants.CENTER);
		fila1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		fila1.setBackground(Color.WHITE);
		fila1.setBounds(69, 329, 50, 21);
		frmJuegosAritmticos.getContentPane().add(fila1);

		JLabel fila2 = new JLabel("??");
		fila2.setForeground(Color.WHITE);
		fila2.setHorizontalAlignment(SwingConstants.CENTER);
		fila2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		fila2.setBackground(Color.WHITE);
		fila2.setBounds(129, 329, 50, 21);
		frmJuegosAritmticos.getContentPane().add(fila2);

		JLabel fila3 = new JLabel("??");
		fila3.setForeground(Color.WHITE);
		fila3.setHorizontalAlignment(SwingConstants.CENTER);
		fila3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		fila3.setBackground(Color.WHITE);
		fila3.setBounds(189, 329, 50, 21);
		frmJuegosAritmticos.getContentPane().add(fila3);

		JLabel fila4 = new JLabel("??");
		fila4.setForeground(Color.WHITE);
		fila4.setHorizontalAlignment(SwingConstants.CENTER);
		fila4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		fila4.setBackground(Color.WHITE);
		fila4.setBounds(249, 329, 50, 21);
		frmJuegosAritmticos.getContentPane().add(fila4);

		JLabel columna1 = new JLabel("??");
		columna1.setHorizontalAlignment(SwingConstants.CENTER);
		columna1.setForeground(Color.WHITE);
		columna1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		columna1.setBackground(Color.WHITE);
		columna1.setBounds(309, 85, 30, 50);
		frmJuegosAritmticos.getContentPane().add(columna1);

		JLabel columna2 = new JLabel("??");
		columna2.setHorizontalAlignment(SwingConstants.CENTER);
		columna2.setForeground(Color.WHITE);
		columna2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		columna2.setBackground(Color.WHITE);
		columna2.setBounds(309, 146, 30, 50);
		frmJuegosAritmticos.getContentPane().add(columna2);

		JLabel columna3 = new JLabel("??");
		columna3.setHorizontalAlignment(SwingConstants.CENTER);
		columna3.setForeground(Color.WHITE);
		columna3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		columna3.setBackground(Color.WHITE);
		columna3.setBounds(309, 207, 30, 50);
		frmJuegosAritmticos.getContentPane().add(columna3);

		JLabel columna4 = new JLabel("??");
		columna4.setHorizontalAlignment(SwingConstants.CENTER);
		columna4.setForeground(Color.WHITE);
		columna4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		columna4.setBackground(Color.WHITE);
		columna4.setBounds(309, 268, 30, 50);
		frmJuegosAritmticos.getContentPane().add(columna4);
	}

}
