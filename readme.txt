Ejemplo de como utilizar GIT y algunos comandos
-----------------------------------------------
Por primera vez, utilizar el comando git clone:

- git clone https://gitlab.com/alanbarraza05/tpprogramacion3.git
- git status (para conocer el estado del proyecto)
- git add . (añade todos los archivos de la carpeta)
- git rm --cached <archivo> (para eliminar un archivo)
- git commit -am "texto de lo hecho" (para preparar los archivos)
- git push origin master (para subir los archivos con el comentario)

Una vez que ya clone, cotidianamente utilizo primero el comando: 

- git pull origin master